import React from 'react'
import { NavLink } from 'react-router-dom'
import { userLocalService } from '../Service/localStorageService'

export default function UserNav() {
    let userData = userLocalService.get()
    let removeUserData = userLocalService.remove

    let handleDangXuat = () => { 
        removeUserData();
        window.location.href = "/"
     }
    const renderUserNav = () => {
        if (userData != null) {
            return (
                <ul className="navbar-nav mx-auto">
                    <li className="nav-item">
                        <i className="glyphicon glyphicon-log-in text-secondary h-10"></i>
                        <NavLink
                            className='nav-link'>
                            <i className="fa fa-user-circle m-1"></i>
                            {userData.hoTen}
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink
                            to={`/`}
                            className='nav-link'
                            onClick={handleDangXuat}
                        >
                            <i className="fa fa-user-circle m-1"></i>
                            Đăng xuất
                        </NavLink>
                    </li>
                </ul>
            )
        } else {
            return (
                <ul className="navbar-nav mx-auto">
                    <li className="nav-item">
                        <i className="glyphicon glyphicon-log-in text-secondary h-10"></i>
                        <NavLink
                            to={`/login`}
                            className='nav-link'>
                            <i className="fa fa-user-circle m-1"></i>
                            Đăng Nhập
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink
                            to={`/signup`}
                            className='nav-link'
                        >
                            <i className="fa fa-user-circle m-1"></i>
                            Đăng ký
                        </NavLink>
                    </li>
                </ul>
            )
        }
    }
    return (
        <>{renderUserNav()}</>
    )
}
