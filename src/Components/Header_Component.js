import React from 'react'
import { NavLink } from 'react-router-dom'
import UserNav from './UserNav'

export default function Header_Component() {
    return (
        <div className='fixed-top'
            style={{ backgroundColor: 'rgba(255, 255, 255, 0.85)', width: "100%", boxShadow: '0 6px 4px 0 rgba(0,0,0,.5)' }}>
            <nav className="navbar navbar-expand-lg navbar-light container" >


                <NavLink
                    to={`/`}
                    className='nav-link'>
                    <a className="navbar-brand">Capstone Movie - Phạm Duy Thắng</a>
                </NavLink>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mx-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="#LichChieuPhim">Lịch chiếu phim</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#CumRap">Cụm rạp</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#UngDung">Ứng dụng</a>
                        </li>
                    </ul>
                    <UserNav/>
                </div>

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
            </nav>
        </div>
    )
}
