import { Image, Progress } from 'antd'
import moment from 'moment'
import React from 'react'

export default function DetailPage_Desc({ Phim }) {

    return (
        <div className='container my-5 mx-auto '
            style={{ color: "rgba(0,0,0,.9)" }}>
            <div className='d-flex justify-content-evenly text-secondary'>
                <div>
                    <Image
                        width={300}
                        height={500}
                        src={Phim.hinhAnh}
                    />
                </div>
                <div className='d-flex flex-column ml-5 align-items-between justify-content-center'>
                    <div className='text-left'>
                            <h1 class="p-2 rounded border border-secondary"
                            >{Phim.tenPhim}</h1>
                        <h3>Ngày khởi chiếu: {moment(Phim.ngayKhoiChieu).format('DD - MM - YYYY')}</h3>
                    </div>
                    <div className='d-flex justify-content-center align-items-center mt-5'>
                        <h1>Đánh giá phim:</h1>
                        <Progress className='px-5' type="circle" percent={Phim.danhGia * 10} format={(percent) => `${percent / 10}`} />
                    </div>
                </div>
            </div>
            <div className='my-3'>
                <p>{Phim.moTa}</p>
            </div>
        </div>
    )
}

// biDanh
// :
// "the-longest-ride-hot"
// dangChieu
// :
// false
// danhGia
// :
// 5
// hinhAnh
// :
// "https://movienew.cybersoft.edu.vn/hinhanh/thelongestride.jpg"
// hot
// :
// true
// maNhom
// :
// "GP07"
// maPhim
// :
// 1470
// moTa
// :
// "After an automobile crash, the lives of a young couple intertwine with a much older man, as he reflects back on a past love."
// ngayKhoiChieu
// :
// "2022-12-24T20:59:16.573"
// sapChieu
// :
// true
// tenPhim
// :
// "The Longest Ride hot"
// trailer
// :
// "https://www.youtube.com/embed/FUS_Q7FsfqU"