import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { Service } from '../../Service/Service';
import DetailPage_Desc from './DetailPage_Desc';
import DetaiPage_Rap from './DetaiPage_Rap';

export default function DetailPage() {
  // gọi ra maPhim
  let maPhim = useParams().idPhim;

  const [Phim, setPhim] = useState({})

  // gọi ra thông tin phim
  useEffect(() => {
    Service
      .getLayThongTinPhim(maPhim)
      .then((res) => {
        console.log("phim", res);
        setPhim(res.data.content)
      })
      .catch((err) => {
        console.log(err);
      })
  }, [])

  return (
    <div
      className='d-flex align-items-center'
      style={{
        background: "url(../img/backapp.b46ef3a1.jpg)",
        backgroundPosition: 'center',
        height: '100vh'
      }}>
      <div
        className='container'
        style={{
          background: "rgba(255, 255, 255, 0.85)",
        }}>
        <DetailPage_Desc Phim={Phim} />
        <DetaiPage_Rap maPhim={maPhim} />
      </div>
    </div>
  )
}
