import React, { useEffect } from 'react'
import { Service } from '../../Service/Service'

export default function DetaiPage_Rap({ maPhim }) {


    useEffect(() => {

        // lấy thông tin lích chiếu phim theo mã phim
        Service
            .getLayThongTinLichChieuPhim(maPhim)
            .then((res) => {
                console.log("thông tin lịch chiếu phim", res.data.content);
            })
            .catch((err) => {
                console.log(err);   
            })
    })


    return (
        <div>

        </div>
    )
}
