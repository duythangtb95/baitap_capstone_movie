import React from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import { useNavigate } from 'react-router-dom';
import { Service } from '../../Service/Service';
import { userLocalService } from '../../Service/localStorageService';


export default function LoginPage() {
  
  const [messageApi, contextHolder] = message.useMessage();
  const success = () => {
    messageApi.open({
      type: 'success',
      content: 'Đã đăng nhập thành công',
      className: 'custom-class',
      style: {
        marginTop: '20vh',
      },
    });
  };

  const error = () => {
    messageApi.open({
      type: 'error',
      content: "Đăng nhập thất bại",
      className: 'custom-class',
      style: {
        marginTop: '20vh',
      },
    });
  };


  let navigate = useNavigate();
  const onFinish = (values) => {
    console.log('Success:', values);
    Service
      .postDangNhap(values)
      .then((res) => {
        console.log(res.data.content);
        let userData = res.data.content;

        // lưu localStorage
        userLocalService.set(userData)

        // thông báo người dùng
        success();

        // chuyển về HomePage
        setTimeout(() => {
          window.location.href = "/";
        }, 1000)
      })

      .catch((err) => {
        console.log(err);
        error()
      })


  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div
      style={{
        height: "100vh",
        background: "url(./img/backapp.b46ef3a1.jpg)",
      }}
      className="d-flex align-items-center justify-content-center">
      <Form
        name="basic"
        labelCol={{
          span: 24,
        }}
        wrapperCol={{
          span: 24,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        style={{
          background: "rgba(255, 255, 255, 0.85)",
        }}
        className="p-5"
      >
        {contextHolder}
        <Form.Item
          label="Tên đăng nhập"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Mật Khẩu"
          name="matKhau"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 4,
            span: 16,
          }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 4,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
