import React from 'react'
import { Outlet } from 'react-router'
import Footer_Component from '../../Components/Footer_Component'
import Header_Component from '../../Components/Header_Component'

export default function HomeTemplate() {
  return (
    <>
      <Header_Component />
      <Outlet />
      <Footer_Component />
    </>
  )
}
