import { Tabs } from 'antd'
import React from 'react'
import Movietabs_2 from './Movietabs_2';

export default function Movietabs_1({ cumrap }) {

  const onChange = (key) => {
    console.log(key);
  }

  let renderlistCumRap = () => {
    return cumrap.map((rap) => {
      return {
        label: (
          <div className='text-start px-0'>
            <h6>{rap.tenCumRap}</h6>
            <h6>{rap.diaChi}</h6>
            <button type="button" className="btn btn-primary">Chi tiết</button>
          </div>
        ),
        key: rap.maCumRap,
        children: (<Movietabs_2 phim={rap.danhSachPhim} />),
      }
    })
  }
  return (
    <div
      className='px-0'
    >
      <Tabs
        style={{
          height: "80vh",
          overflow: "auto",
        }}
        defaultActiveKey="1"
        tabPosition='left'
        onChange={onChange}
        items={renderlistCumRap()}
      />
    </div>
  )
}







