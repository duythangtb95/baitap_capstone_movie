import React from 'react'
import { Card } from 'antd';
import { NavLink } from 'react-router-dom';
const { Meta } = Card;

export default function MovieList({ movielist }) {

    const renderMovidelist = () => {
        return movielist.map((item) => {
            return (
                <div className='col-3 my-1 d-flex justify-content-center' key={item.maPhim}>
                    <Card
                        hoverable
                        style={{
                            width: 240,
                        }}
                        cover={
                            <img
                                style={{
                                    height: 356,
                                }}
                                alt="example" src={item.hinhAnh} />
                        }>
                        <Meta title={item.tenPhim} className='my-1' />
                        <NavLink
                            className='btn btn-danger'
                            to={`/detail/${item.maPhim}`}>
                            Xem chi tiết
                        </NavLink>
                    </Card>
                </div>
            )
        })
    }

    return (
        <div className='row container mx-auto px-0 text-center' >
            {renderMovidelist()}
        </div>
    )
}


// biDanh: "wednesday123231231323"
// dangChieu:true
// danhGia:9
// hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/wednesday_gp01.jpg"
// hot:true
// maNhom:"GP01"
// maPhim:11320
// moTa:"wednesday phim truyền hình nhiều tập Netflix"
// ngayKhoiChieu:"2023-01-04T18:51:52.913"
// sapChieu: true
// tenPhim: "wednesday123231231323"
// trailer: "https://youtu.be/Q73UhUTs6y0"