import React, { useEffect, useState } from 'react'
import { Tabs } from 'antd';
import { Service } from '../../Service/Service';
import Movietabs_1 from './Movietabs_1';


export default function Movietabs() {

    const onChange = (key) => {
        console.log(key);
    }

    const [raplist, setRaplist] = useState([])


    useEffect(() => {
        Service
            .getLayThongTinLichChieuHeThongRap()
            .then((res) => {
                console.log("thong tin hệ thống rạp: ", res.data.content);
                setRaplist(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])

    const renderlistRap = () => {
        return raplist.map((hethongrap) => {
            return {
                label: <img
                    style={{
                        height: 35,
                    }}
                    alt="example" src={hethongrap.logo} />,
                key: hethongrap.maHeThongRap,
                children: (<Movietabs_1 cumrap={hethongrap.lstCumRap} />),
            }
        })
    }
    return (
        <div 
        id='CumRap'
        className='container my-5 p-0 border border-secondary'
            style={{
                height: "80vh",
                overflow: "hidden",
            }}>
            <Tabs
                defaultActiveKey="1"
                tabPosition='left'
                onChange={onChange}
                items={renderlistRap()}
            />
        </div>
    )
}

