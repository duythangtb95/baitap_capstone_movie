import moment from 'moment/moment';
import React from 'react'


export default function Movietabs_2({ phim }) {

    let renderlistPhim = () => {
        return phim.map((item) => {
            return (
                <div className='row' key={item.maPhim}>
                    <div className='my-1 col-4'
                        style={{
                            background: `url(${item.hinhAnh})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                            height: "200px",
                        }}
                    >
                    </div>
                    <div className='col-8'>
                        <h6>{item.tenPhim}</h6>
                        <div className='row'>
                            {item.lstLichChieuTheoPhim.map((first, index) => {
                                return <div className='col-6 mb-1' key={index}>
                                    {moment(first.ngayChieuGioChieu).format("DD/MM/YYYY - h:mm a")}
                                </div>
                            })}
                        </div>
                    </div>
                </div>
            )
        })
    };

    console.log("phim", phim)
    return (
        <div>
            {renderlistPhim()}
        </div>
    )
}
