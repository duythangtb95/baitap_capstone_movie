import React from 'react'
import { Carousel } from 'antd';




export default function Banner_Component({ moviebanner }) {

    const renderBanner = () => {
        return moviebanner.map((item) => {
            const contentStyle1 = {
                margin: 0,
                color: '#CCCCCC',
                lineHeight: '160px',
                textAlign: 'center',
                backgroundImage: `url(${item.hinhAnh})`,
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                width: '100vw',
                height: '100vh'
            };
            return (
                <div key={item.maBanner}>
                    <h3 style={contentStyle1}>
                    </h3>
                </div>
            )
        })
    }

    // const onChange = (moviebanner) => {
    //     console.log(moviebanner);
    // }
    //  afterChange={onChange}

    return (
        <Carousel autoplay>
            {renderBanner()}
        </Carousel>
    )
}


