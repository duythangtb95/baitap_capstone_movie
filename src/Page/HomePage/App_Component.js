import { Carousel } from 'antd';
import React from 'react'

export default function App_Component() {

    const contentStyle = [{
        background: 'url(./img/app_slider1.jpg)',
        height: "400px",
        with: "200px",
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    }, {
        background: 'url(./img/app_slider2.jpg)',
        height: "400px",
        with: "200px",
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    }, {
        background: 'url(./img/app_slider3.jpg)',
        height: "400px",
        with: "200px",
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    }, {
        background: 'url(./img/app_slider4.jpg)',
        height: "400px",
        with: "200px",
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    }, {
        background: 'url(./img/app_slider5.jpg)',
        height: "400px",
        with: "200px",
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    }, {
        background: 'url(./img/app_slider6.jpg)',
        height: "400px",
        with: "200px",
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    }
    ]

    const renderApplider = () => {
        return contentStyle.map((item,index) => {
            return (
                <div key={index}>
                    <div style={item}></div>
                </div>
            )
        })

    }

    return (
        <div
        id='UngDung'
            className='d-flex align-items-center justify-content-center'
            style={{
                backgroundImage: `url(./img/backapp.b46ef3a1.jpg)`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                height: '500px'
            }}>
            <div className='row text-white container justify-content-evenly align-items-center my-auto'>
                <div className='col-6 col-sm'>
                    <h4>Ứng dụng tiện lợi dành cho</h4>
                    <h4>người yêu điện ảnh</h4>
                    <p>Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi quà hấp dẫn.</p>
                    <a className="btn btn-primary" href="#" role="button">App miễn phí - Tải về ngay!</a>
                    <p>TIX có hai phiên bản</p>
                    <a className="btn btn-primary mx-1" href="#" role="button">IOS</a>
                    <span>&</span>
                    <a className="btn btn-primary mx-1" href="#" role="button">ANDROID</a>
                </div>
                <div className='col-6 col-sm p-0'
                    style={{
                        backgroundImage: `url(./img/background_app.png)`,
                        backgroundRepeat: 'no-repeat',
                        backgroundSize: 'contain',
                        height: '400px',
                        width: '200px',
                    }}>
                    <Carousel autoplay>
                        {renderApplider()}
                    </Carousel>
                </div>
            </div>
        </div>
    )
}

