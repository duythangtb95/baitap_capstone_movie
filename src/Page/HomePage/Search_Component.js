import React, { useEffect, useState } from 'react'
import { Service } from '../../Service/Service';
import { Select } from 'antd';
import moment from 'moment/moment';

export default function Search_Component({ movielist }) {

    let listMovieName = movielist.map((item) => {
        return ({
            tenPhim: item.tenPhim,
            maPhim: item.maPhim
        })
    });
    console.log("listMovieName: ", listMovieName)

    // defaultvalue
    const [Movie, setMovie] = useState("Chọn Rạp")
    const [GioChieu, setGioChieu] = useState("Chọn giờ chiếu")

    // danh sách để render
    const [listRap, setListRap] = useState([])
    const [listGioChieu, setListGioChieu] = useState([])


    // handleMoiveChange
    const handleMovieChange = (value) => {

        // lấy danh sách rạp
        Service
            .getLayThongTinLichChieuPhim(value)
            .then((res) => {

                console.log("giờ chiếu: ", res.data.content.heThongRapChieu[0].cumRapChieu)
                let listRap2 = res.data.content.heThongRapChieu.map((item) => {
                    return ({
                        label: item.tenHeThongRap,
                        options: item.cumRapChieu.map((rap) => {
                            return {
                                label: rap.tenCumRap,
                                value: rap.maCumRap,
                                lichchieu: rap.lichChieuPhim
                            }
                        })
                    })
                })
                setListRap(listRap2)
                console.log("lichchieuphim", listRap2);

            })
            .catch((err) => {
                console.log(err);
            })
        setMovie("Chọn Rạp")
        setGioChieu("Chọn giờ chiếu")

    }

    // handleRapChange
    const handleRapChange = (value, lichchieu) => {
        console.log("lịch chiếu đây nè: ", lichchieu.lichchieu.map((item) => {
            return {
                value: item.maLichChieu,
                label: item.ngayChieuGioChieu
            }
        }))

        let listGioChieu2 = lichchieu.lichchieu.map((item) => {
            return {
                value: item.maLichChieu,
                label: moment(item.ngayChieuGioChieu).format("DD/MM/YYYY - h:mm a")
            }
        })
        setListGioChieu(listGioChieu2)
        setMovie(value)
        setGioChieu("Chọn giờ chiếu")
    }
    const handleGioChieuChange = (value) => {
        console.log(value)
        setGioChieu(value)
    }



    return (
        <div
            id='LichChieuPhim'
            className='container my-2 px-0 d-flex justify-content-between align-content-center'
        >
            <Select
                defaultValue={"Chọn Phim"}
                style={{
                    width: "30%",
                }}
                onChange={handleMovieChange}
                options={listMovieName.map((province) => ({
                    label: province.tenPhim,
                    value: province.maPhim,
                }))}
            />
            <Select
                style={{
                    width: "30%",
                }}
                value={Movie}
                onChange={handleRapChange}
                options={listRap}
            />
            <Select
                style={{
                    width: "30%",
                }}
                value={GioChieu}
                onChange={handleGioChieuChange}
                options={listGioChieu}
            />

            <button type="button" className="btn btn-primary text-alig"
                style={{
                    height: "32px",
                }}>
                Đặt Vé</button>

        </div>
    )
}





