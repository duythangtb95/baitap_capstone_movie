import React, { useEffect, useState } from 'react'
import { Service } from '../../Service/Service'
import App_Component from './App_Component';
import Banner_Component from './Banner_Component';
import MovieList from './MovieList';
import Movietabs from './Movietabs';
import Search_Component from './Search_Component';


export default function HomePage() {

  // danh sách phim 
  const [movielist, setMovielist] = useState([]);

  // danh sách banner
  const [moviebanner, setMoviebanner] = useState([]);

  useEffect(() => {
    //  lấy danh sách phim
    Service
      .getLayDanhSachPhim()
      .then((res) => {
        console.log("movielist:", res);
        setMovielist(res.data.content)
      })
      .catch((err) => {
        console.log(err);
      });

    // lấY danh sách banner
    Service
      .getLayDanhSachBanner()
      .then((res) => {
        console.log("banner:", res);
        setMoviebanner(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });

    // lấy danh sách rạp chiếu phim

  }, [])

  // show danh sách phim

  return (
    <div>
      <Banner_Component className="carousel" moviebanner={moviebanner} />
      <Search_Component movielist={movielist} />
      <MovieList movielist={movielist} />
      <Movietabs />
      <App_Component />
    </div>
  )
}
