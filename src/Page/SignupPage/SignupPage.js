import React, { useState } from 'react'
import {
  Button,
  Checkbox,
  Form,
  Input,
  message,
} from 'antd';
import { Service } from '../../Service/Service';
import { userLocalService } from '../../Service/localStorageService';





export default function SignupPage() {

  const [messageApi, contextHolder] = message.useMessage();
  const success = () => {
    messageApi.open({
      type: 'success',
      content: 'Đã đăng ký thành công',
      className: 'custom-class',
      style: {
        marginTop: '20vh',
      },
    });
  };

  const error = (value) => {
    messageApi.open({
      type: 'error',
      content: value,
      className: 'custom-class',
      style: {
        marginTop: '20vh',
      },
    });
  };

  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

  function formDangKy(taiKhoan, matKhau, email, soDt, hoTen) {
    this.taiKhoan = taiKhoan;
    this.matKhau = matKhau;
    this.email = email;
    this.soDt = soDt;
    this.hoTen = hoTen;
  }

  // {
  //   "taiKhoan": "string",
  //   "matKhau": "string",
  //   "email": "string",
  //   "soDt": "string",
  //   "maNhom": "string",
  //   "hoTen": "string"
  // }
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    // let [email,hoTen,matKhau,phone,taiKhoan] = values;

    // lấy thông tin kh đăng kí
    let newUserDangKy = new formDangKy(values.taiKhoan, values.matKhau, values.email, values.phone, values.hoTen)

    console.log(newUserDangKy)
    // console.log(matKhau)
    // console.log(email)
    // console.log(phone)
    // console.log(hoTen)



    Service
      .getDangKy(newUserDangKy)
      .then((res) => {
        console.log(res);
        success();
        userLocalService.set(newUserDangKy)

        setTimeout(() => {
          window.location.href = "/";
        }, 1000)
      })
      .catch((err) => {
        console.log(err.response.data.content);
        error(err.response.data.content);
      })

  };


  return (
    <div
      style={{
        height: "100vh",
        background: "url(./img/backapp.b46ef3a1.jpg)",
      }}
      className='d-flex align-items-center justify-content-center'>

        {/* thông báo của messesage */}
        {contextHolder}
      <Form
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={onFinish}
        style={{
          background: "rgba(255, 255, 255, 0.85)",
        }}
        className="p-5"
      >
        {/* Email */}
        <Form.Item
          name="email"
          label="E-mail"
          rules={[
            {
              type: 'email',
              message: 'E-mail không đúng!',
            },
            {
              required: true,
              message: 'Mời nhập emai !',
            },
          ]}
        >
          <Input />
        </Form.Item>

        {/* Họ tên */}
        <Form.Item
          name="hoTen"
          label="Họ tên"
          tooltip="Bạn tên gì?"
          rules={[
            {
              required: true,
              message: 'Mời nhập họ tên !',
              whitespace: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        {/* tài khoản */}
        <Form.Item
          name="taiKhoan"
          label="Tài khoản"
          tooltip="Tên khoản của bạn"
          rules={[
            {
              required: true,
              message: 'Mời nhập tên tài khoản!',
              whitespace: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        {/* mật khẩu */}
        <Form.Item
          name="matKhau"
          label="Mật khẩu"
          rules={[
            {
              required: true,
              message: 'Mời nhập mất khẩu',
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        {/* xác nhận mật khẩu */}
        <Form.Item
          name="confirm"
          label="Confirm Password"
          dependencies={['password']}
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Mời xác nhật mật khẩu!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('matKhau') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('Hai mật khẩu không trùng nhau!'));
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>


        {/* số điện thoại */}
        <Form.Item
          name="phone"
          label="Phone Number"
          rules={[
            {
              required: true,
              message: 'Please input your phone number!',
            },
          ]}
        >
          <Input
            style={{
              width: '100%',
            }}
          />
        </Form.Item>



        <Form.Item
          name="agreement"
          valuePropName="checked"
          rules={[
            {
              validator: (_, value) =>
                value ? Promise.resolve() : Promise.reject(new Error('Mời bạn đồng ý với tài khoản')),
            },
          ]}
          {...tailFormItemLayout}
        >
          <Checkbox>
            I have read the <a href="">agreement</a>
          </Checkbox>
        </Form.Item>


        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}




