import React from 'react';
import './NotfoundPage.css';

export default function NotfoundPage() {
  return (
    <div
      className='d-flex justify-content-center align-items-center'
      style={{
        height: "100vh",
        background: "url(./img/backapp.b46ef3a1.jpg)",
      }}>
      <div
      className='p-5'
        style={{
          background: "rgba(255, 255, 255, 0.85)",
        }}>
        <h1>404 Error Page</h1>
        <p className="zoom-area"><b>CSS</b> animations to make a cool 404 page. </p>
        <section className="error-container">
          <span className="four"><span className="screen-reader-text">4</span></span>
          <span className="zero"><span className="screen-reader-text">0</span></span>
          <span className="four"><span className="screen-reader-text">4</span></span>
        </section>
        <div className="link-container">
          <a target="_blank" href="https://www.silocreativo.com/en/creative-examples-404-error-css/" className="more-link">Visit the original article</a>
        </div>
      </div>
    </div>
  )
}


