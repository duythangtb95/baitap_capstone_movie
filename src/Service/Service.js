import axios from "axios"
import { Base_URL, Token_Cybersoft } from "./ConfigURL"

export const Service = {
        //  post đăng nhập
    postDangNhap: (values) => {
        return axios({
            method: "POST",
            url: `${Base_URL}/api/QuanLyNguoiDung/DangNhap`,
            headers: Token_Cybersoft(),
            data: values,
        })
    },

        //  lấy danh sách phim
    getLayDanhSachPhim: () => {
        return axios({
            method: "GET",
            url: `${Base_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07`,
            headers: Token_Cybersoft(),
        })
    },

    // lấY danh sách banner
    getLayDanhSachBanner: () => {
        return axios({
            method: "GET",
            url: `${Base_URL}/api/QuanLyPhim/LayDanhSachBanner`,
            headers: Token_Cybersoft(),
        })
    },

    // lấy danh sách rạp chiếu phim
    getLayThongTinLichChieuHeThongRap: () => {
        return axios({
            method: "GET",
            url: `${Base_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP07`,
            headers: Token_Cybersoft(),
        })
    },

    // lấy thông tin phim theo mã phim
    getLayThongTinPhim: (maPhim) => {
        return axios({
            method: "GET",
            url: `${Base_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`,
            headers: Token_Cybersoft(),
        })
    },
    
    // LayThongTinLichChieuPhim
    getLayThongTinLichChieuPhim: (maPhim) => {
        return axios({
            method: "GET",
            url: `${Base_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`,
            headers: Token_Cybersoft(),
        })
    },

    // DangKy
    getDangKy: (ttUser) => {
        return axios({
            method: "POST",
            url: `${Base_URL}/api/QuanLyNguoiDung/DangKy`,
            headers: Token_Cybersoft(),
            data: ttUser,
        })
    },
    
}