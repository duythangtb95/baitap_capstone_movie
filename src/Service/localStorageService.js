export const USER_LOGIN = "USER_LOGIN";

export const userLocalService ={
    set:(userData) => { 
        let userJson = JSON.stringify(userData);
        localStorage.setItem(USER_LOGIN, userJson)
     },
     get: () => { 
        let userJon = localStorage.getItem(USER_LOGIN);
        if(userJon !=null){
            return JSON.parse(userJon);
        } else{
            return null;
        }
      },
      remove: () => { 
        localStorage.removeItem(USER_LOGIN)
       },
}