import './App.css';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import HomePage from './Page/HomePage/HomePage';
import DetailPage from './Page/DetailPage/DetailPage';
import LoginPage from './Page/LoginPage/LoginPage';
import SignupPage from './Page/SignupPage/SignupPage';
import NotfoundPage from './Page/NotfoundPage/NotfoundPage';
import HomeTemplate from './Page/Template/HomeTemplate';
import DemoPage from './Page/Demo/DemoPage';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='' element={<HomeTemplate />}>
            <Route path="/" element={<HomePage />} />
            <Route path='/Detail/:idPhim' element={<DetailPage />} />
            <Route path='/Login' element={<LoginPage />} />
            <Route path='/Signup' element={<SignupPage />} />
            <Route path='/notfound' element={<NotfoundPage/>}/>
            <Route path='*' element={<Navigate to='/notfound'/>} />
          </Route>

          <Route path='/Demo' element={<DemoPage />} />
        </Routes>
      </BrowserRouter>


    </div>
  );
}

export default App;
